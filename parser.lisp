(in-package #:sediment.parser)

(defstruct parser
  result
  (source "" :type (simple-array character)))
