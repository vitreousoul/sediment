;;;; sediment.asd

(asdf:defsystem #:sediment
  :description "Parser combinators"
  :author "Ryan Merritt <merrittaudio@gmail.com>"
  :license  "BSD 2 Clause"
  :version "0.0.1"
  :serial t
  :depends-on (:alexandria)
  :components ((:file "package")
	       (:file "parser")
	       (:file "primitives")
	       (:file "utils")
	       (:file "bind")
	       (:file "combinators")
	       (:file "sediment")))
