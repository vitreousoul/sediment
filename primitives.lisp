(in-package #:sediment.primitives)

(defun result (v)
  (lambda (inp)
    (list (make-parser
	   :result v
	   :source inp))))

(defun zero (inp)
  (declare (ignore inp))
  nil)

(defun item (inp)
  (cond
    ((string-equal inp "") nil)
    (t (list (make-parser
	      :result (subseq inp 0 1)
	      :source (subseq inp 1))))))
