(in-package #:sediment.combinators)

(defun sat (predicate)
  (bind #'item
	(lambda (x)
	  (if (funcall predicate x)
	      (result x)
	      #'zero))))

(defun char-eq (x)
  (sat (lambda (y)
	 (string-equal x y))))

(defun digit? ()
  (sat (lambda (x)
	 (and (string<= x "9")
	      (string>= x "0")))))

(defun lower? ()
  (sat (lambda (x)
	 (and (string<= x "z")
	      (string>= x "a")))))

(defun upper? ()
  (sat (lambda (x)
	 (and (string<= x "Z")
	      (string>= x "A")))))

;; TODO: plus could accept more than two parsers
(defun plus (p q)
  (lambda (inp)
    (concatenate 'list
		 (funcall p inp)
		 (funcall q inp))))

(defun letter? ()
  (plus (funcall #'lower?)
	(funcall #'upper?)))

(defun alphanum? ()
  (plus (funcall #'letter?)
	(funcall #'digit?)))

(defun word? ()
  (let ((any-word (bind (letter?)
			(lambda (x)
			  (bind (word?)
				(lambda (xs)
				  (result (concatenate 'string x xs))))))))
    (plus any-word
	  (funcall #'result ""))))

'(defun string? (str)
  (cond ((string-equal "" str) (result ""))
	 (t (let ((x (subseq str 0 1))
		  (xs (subseq str 1)))
	      (binding (lambda (a b)
			 (concatenate 'string a b))
		       (char-eq x)
		       (string? xs))))))
