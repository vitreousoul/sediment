(in-package #:sediment.bind)

(defun resolve-parser (p)
  (if (symbolp p)
      (symbol-function p)
      p))

(defun resolve-parser-function (f)
  (if (functionp f)
      f
      (eval f)))

(defmacro bind-chain (result parsers-symbols)
  (cond ((emptyp parsers-symbols) result)
	(t (destructuring-bind (parser . lambda-symbol)
	       (first parsers-symbols)
	     `(bind (funcall ,parser)
		    (lambda (,lambda-symbol)
		      (bind-chain ,result ,(rest parsers-symbols))))))))

(defmacro binding (f &rest parser-symbols)
  "Allows representing nested binds in a flat, concise way.
   With explicit, nested binds:
     (bind parser1
           (lambda (p1)
             (bind parser2
                   (lambda (p2)
                     (result (f p1 p2))))))
   Using binding macro:
     (binding f parser1 parser2)"
  (let* ((parsers (mapcar (lambda (p)
			    (symbol-function p))
			  parser-symbols))
	 (lambda-symbols (mapcar (lambda (s)
				   (symbolicate "l-" s))
				 parser-symbols))
	 (pairs (loop
		   :for p :in parsers
		   :for l :in lambda-symbols
		   :collect (cons p l))))
    `(bind-chain 
      (result (funcall ,(if (symbolp f)
			    (symbol-function f)
			    f)
		       ,@lambda-symbols))
      ,pairs)))

(defun bind (p f)
  (flet ((handler (parser)
	   (funcall
	    (funcall f (parser-result parser))
	    (parser-source parser))))
    (lambda (inp)
      (concat
       (mapcar #'handler (funcall p inp))))))
