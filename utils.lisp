(in-package #:sediment.utils)

(defun concat (lists)
  (apply 'concatenate 'list lists))
