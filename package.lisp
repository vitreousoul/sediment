;;;; package.lisp

(uiop:define-package #:sediment.utils
    (:use #:cl)
  (:export :concat))

(uiop:define-package #:sediment.parser
    (:use #:cl)
  (:export :parser
	   :make-parser
	   :parser-result
	   :parser-source))

(uiop:define-package #:sediment.primitives
    (:use #:cl
	  #:sediment.parser)
  (:export :result
	   :zero
	   :item))

(uiop:define-package #:sediment.bind
    (:use #:cl
	  #:alexandria
	  #:sediment.parser
	  #:sediment.primitives
	  #:sediment.utils)
  (:export :binding
	   :bind))

(uiop:define-package #:sediment.combinators
    (:use #:cl
	  #:sediment.parser
	  #:sediment.primitives
	  #:sediment.bind)
  (:export :zero
	   :item
	   :sat
	   :char-eq
	   :digit?
	   :lower?
	   :upper?
	   :plus
	   :letter?
	   :alphanum?
	   :word?
	   :string?))

(uiop:define-package #:sediment
    (:use #:cl
	  #:sediment.parser
	  #:sediment.primitives
	  #:sediment.bind
	  #:sediment.combinators)
  (:reexport #:sediment.parser
	     #:sediment.primitives
	     #:sediment.bind
	     #:sediment.combinators))
